clear all; close all; clc; format longG;

x0 = 20;
tol = 1e-10;

[x, no_of_iter] = newton_method_for_loop(x0, tol);
fprintf('The final value of x after %d iterations is %f\n', no_of_iter, x);