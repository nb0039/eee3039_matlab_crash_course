function [xf, no_of_iterations] = newton_method_for_loop(x0, tol)

% retrieve initial guess
xn = x0;

for ii = 1:10

    fxn = sin(xn) + xn*cos(xn);             % evaluate function in xn
    fpxn = cos(xn) + cos(xn) - xn*sin(xn);  % evaluate first derivative in xn

    % Print output
    fprintf('xn: %f, |f(xn)|: %e\n', xn, abs(fxn));

    % check for convergence using IF statement
    if(abs(fxn) < tol)
        break;
    else
        % Newton's method
        dxn = -fxn/fpxn;                        % delta x_n
        xn = xn + dxn;                          % update value of xn
    end
end

% output variables
xf = xn;
no_of_iterations = ii;