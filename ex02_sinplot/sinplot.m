clear all; close all; clc; format longG;

x = linspace(0, 2*pi, 100);
y = sin(x);

figure()
plot(x, y);