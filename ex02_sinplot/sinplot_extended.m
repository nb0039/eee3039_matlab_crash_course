clear all; close all; clc; format longG;

x = linspace(0, 2*pi, 100);
y1 = sin(x);
y2 = cos(x);

figure(), hold on, grid on;
plot(x, y1, 'b-o', 'LineWidth', 2);
plot(x, y2, 'r-o', 'LineWidth', 2);
xlabel('x [-]')
ylabel('y [-]')
title('Harmonic Functions')
legend('Sine','Cosine')
