clear all; close all; clc; format longG;


a = 10;

%% IF Statement
if(a > 5)
    fprintf('a is greater than 5!\n');
end


%% IF-ELSE statement
if(a < 5)
    fprintf('a is less than 5!\n');
else
    fprintf('a is greater than 5!\n');
end


%% IF-ELSEIF-...-ELSE Statement
if(a < 5)
    fprintf('a is less than 5!\n');
elseif(a >=5 && a < 10)
    fprintf('a is greater or equal than 5, but less then 10\n');
else
    fprintf('a is greater or equal than 10!\n');
end

