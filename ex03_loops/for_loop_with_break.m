clear all; close all; clc; format longG;

b = 10;

for ii = 1:10
    fprintf('Value of b is %d\n', b);
    b = b + 5;
    if(b >= 50)
        break;
    end
end
fprintf('For loop has finished\n');