clear all; close all; clc; format longG;

% create a 4x3 matrix with normally distributed elements
A = [1.3, 1.2, 1.3; 
     3.1, 2.4, 3.1; 
     4.1, 4.2, 4.3; 
     7.1, 8.4, 9.1];

% loop over rows
for ii = 1:4

    % loop over columns
    for jj = 1:3

        fprintf('A(%d,%d) = %f\n',ii, jj, A(ii,jj));
    end
end

