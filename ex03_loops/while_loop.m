clear all; close all; clc; format longG;

a = 10;

while(a < 50)
    fprintf('Value of a is %d\n', a);
    a = a + 5;
end
fprintf('while loop has finished\n');