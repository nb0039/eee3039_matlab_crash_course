clear all; close all; clc; format longG;

base = 5;   % m
height = 10;    % m

a = triArea(base, height);

fprintf('Area of triangle is %f m^2\n', a);