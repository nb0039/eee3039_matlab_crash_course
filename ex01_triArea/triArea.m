function a = triArea(b, h)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% a = triArea(b,h)
% 
% Calculate the area of a triangle
% 
% INPUT:
% - b   base of triangle
% - h   height of triangle
% 
% OUTPUT:
% - a   area of triangle
%
% Author: N. Baresi
% Date: Sep 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

base = b;
height = h;

a = base*height/2;
end