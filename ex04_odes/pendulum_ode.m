function dXdt = pendulum_ode(t, X, l, g)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% dXdt = pendulum_ode(t, X, l, g)
%
% System of first-order ODEs for the pendulum problem
% 
% INPUTS:
% - t   time
% - X   state vetor [angular position, angular velocity]
% - l   pendulum length [m]
% - g   gravitational acceleration [m/s^2]
%
% OUTPUTS:
% - dXdt    time derivative of state vector [angular vel., angular acc.]
%
% Author: N. Baresi
% Date: Sep 2023
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Retrieve inputs
theta = X(1);
omega = X(2);

% initialize output
dXdt = zeros(2,1);

% Differential equations
dXdt(1) = omega;
dXdt(2) = -g/l*sin(theta);

end
