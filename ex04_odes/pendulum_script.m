clear all; close all; clc; format longG;
set(0, 'DefaultTextInterpreter', 'latex');
set(0, 'DefaultAxesFontSize', 20);
set(0, 'DefaultLineLineWidth', 2);

l = 1.0;     % pendulum length (m)
g = 9.81;    % gravitational acceleration (m/s^2)

% Set initial conditions
theta0 = pi/8;
omega0 = 0.1;
X0 = [theta0; omega0];

% Set time vector
tstart = 0;
tend = 10*2*pi*sqrt(l/g);
tspan = [tstart, tend];


% Ode-options
ode_opt = odeset('RelTol', 3e-12, 'AbsTol', 1e-14);


% Call to ODE45 (replace with ODE113 if you prefer)
[tout, Xout] = ode45(@pendulum_ode, tspan, X0, ode_opt, l, g);


% Store output in new variables
time = tout;
thetat = Xout(:, 1);   % Recall: X = [theta, omega]
omegat = Xout(:, 2);


%% Plot outputs
figure()
yyaxis left;
plot(time, thetat);
xlabel('Time (s)');
ylabel('$\theta$ (rad)');
xlim([0, time(end)]);
ylim([1.2*min(thetat), 1.2*max(thetat)]);
box on; grid on;

yyaxis right;
plot(time, omegat);
ylabel('$\omega$ (rad/s)');
ylim([1.2*min(omegat), 1.2*max(omegat)]);
box on; grid on;
savefig('pendulum_plots.fig');
print('pendulum_plots.png','-dpng');


%% Computing the Energy and Speed Comparison
disp('Energy Calculation: For Loop');
tic
E_vec = zeros(length(thetat), 1);
for ii = 1:length(thetat)
    E_vec(ii) = 0.5*omegat(ii)^2 - (g/l)*cos(thetat(ii));
end
toc

disp('Energy Calculation: Vectorised code');
tic
E_vec = 0.5*(omegat.^2) - (g/l)*cos(thetat);
toc

