clear all; close all; clc; format longG;
set(0, 'DefaultTextInterpreter', 'latex');
set(0, 'DefaultAxesFontSize', 20);
set(0, 'DefaultLineLineWidth', 2);


% Define problem parameters
k = 
m = 


% Define initial conditions
x = 
v =
X = 


% Define time vector
t0 =
tf =
tspan


% Ode-options
ode_opt = odeset('RelTol', ,'AbsTol', );


% Call ode113
[tout, Xout] = ode113();



% store output in new variables
time = tout;
x =
v = 


% Plot output
figure()
savefig('springmass_plots.fig');
print('springmass_plots.png', '-dpng');


% Calculate Energy


